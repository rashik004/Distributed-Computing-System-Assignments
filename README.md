# Distributed Computing System Assignment, Fall 2020

**Name: Rashik Hasnat**

**Student Id: 20266010**

**Mail: rashik.hasnat@g.bracu.ac.bd, rhasnat93@gmail.com**

## Assignment 3 (Individual submission on Access/Shell)
I've uploaded the video on youtube. Please click on the thumbnail below or click [this link](https://www.youtube.com/watch?v=NagP8jgUFks) to see the video.

[![Introduction to Using the Shell in a High-Performance Computing Context ](https://img.youtube.com/vi/NagP8jgUFks/0.jpg)](https://www.youtube.com/watch?v=NagP8jgUFks)

